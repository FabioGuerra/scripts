#!/bin/sh
set -eu

cd "$HOME/Nuvola"

out="$1/Nuvola/$(date -Iseconds).tar.zst"

mkdir -p "$1/Nuvola"

tar --zstd -p -cf "$out" ./

cd - > /dev/null
