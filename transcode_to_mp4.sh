#!/bin/bash
set -eu

lst="$1" # List of files to transcode to mp4 (relative paths in $src)
src="$2" # Source directory
dst="$3" # Destination directory

mkdir -p "$dst"

while read -u 10 f; do
	mkdir -p "${dst}/$(dirname "$f")"

	if [[ "$f" == *.mp4 ]]; then
		cp "${src}/${f}" "${dst}/${f}"
	else
		ffmpeg -i "${src}/${f}" "${dst}/temp.mp4"
		mv "${dst}/temp.mp4" "${dst}/${f%.*}.mp4" 
	fi
done 10< "$lst"
