#!/bin/sh
set -eu

cd "$HOME"

out="$1/$(cat /etc/hostname)/$(date -Iseconds).tar.zst"

mkdir -p "$1/$(cat /etc/hostname)"

tar --zstd -p --xattrs --acls -cf "$out" \
  --exclude ./.cache \
  --exclude ./Nuvola \
  --exclude ./pCloudDrive \
  --exclude ./.local/share/containers/storage \
  --exclude ./.config/Cryptomator/ipc.socket \
  ./

cd - > /dev/null
